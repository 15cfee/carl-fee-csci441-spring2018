This program will allow a user to move an object around a maze. View modes can be toggled
by pressing the spacebar. In the ortho view mode, the arrow keys are used to move the
object around on the maze. When the view is switched to projection, the camera moves in to where the object is. The
arrows are still used to move the object, just now the object is followed by the camera. In this mode,
the wasd keys can be used to orient the camera in different directions. However, the arrow keys still move the
object in the same direction, not necesarily the direction the camera is pointed in.

The main place that I utilized creativity was the maze. I made a method that created sqaures
that corresponded to X's in a text file. It made it super easy to edit the maze. The maze
also used containers to mark areas that the bunny should not enter. It was a very simple design and fun to
implement. I also used more methods and broke code out into more files in this project in comparison to the
last project.

