//
// Created by fee_c on 3/25/2018.
// https://www.uow.edu.au/~lukes/TEXTBOOK/notes-cpp/io/readtextfile.html
// some code from https://stackoverflow.com/questions/13711466/scanning-information-from-file-in-c
// https://stackoverflow.com/questions/236129/the-most-elegant-way-to-iterate-the-words-of-a-string
// https://stackoverflow.com/questions/4207737/how-to-convert-stdstring-to-const-char
//https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
#ifndef GLFW_EXAMPLE_LOADMODEL_H
#define GLFW_EXAMPLE_LOADMODEL_H
#include <fstream>
#include <math.h>

using namespace std;

class Load {

public:
    Model load(string file){
        string line;
        ifstream input(file);
        vector<Vector> points;
        vector<float> coords;
        float maxX = -50000;
        float maxY = -50000;
        float maxZ = -50000;
        float minX = 50000;
        float minY = 50000;
        float minZ = 50000;
        while(std::getline(input, line))
        {
            if(line[0] == '#') {
                continue;
            } else if(line.length() > 1 && line[0] == 'v' && line[1] != 'n'){
                istringstream iss(line);
                vector<float> temp;
                do {
                    string sub;
                    iss >> sub;
                    if(sub.size() > 0 && sub[0] != 'v'){
                        float num = strtof(sub.c_str(), nullptr);
                        temp.push_back(num);
                    }
                } while(iss);
                points.push_back(Vector(temp[0],temp[1],temp[2]));
                if(temp[0] > maxX) maxX = temp[0];
                if(temp[0] < minX) minX = temp[0];
                minY = min(minY,temp[1]);
                maxY = max(maxY,temp[1]);
                minZ = min(minZ,temp[2]);
                maxZ = max(maxZ,temp[2]);
            } else if(line[0] == 'f') {
                istringstream iss(line);
                int one;
                int two;
                int three;
                int count = 1;
                do {
                    string sub;
                    iss >> sub;
                    if(sub[0] != 'f'){
                        char delimiter = '/';
                        std::vector<std::string> tokens;
                        std::string token;
                        std::istringstream tokenStream(sub);
                        std::getline(tokenStream, token, delimiter);
                        if(count == 1){
                            one = (int)strtof(token.c_str(), nullptr);
                        } else if(count == 2){
                            two = (int)strtof(token.c_str(), nullptr);
                        } else if(count == 3){
                            three = (int)strtof(token.c_str(), nullptr);
                        }
                        count++;
                    }
                } while(iss);
                Vector p1 = points[one -1];
                Vector p2 = points[two -1];
                Vector p3 = points[three -1];
                Vector norm = (p2 - p1).cross(p3 - p2).normalized();

                coords.push_back(p1.x());
                coords.push_back(p1.y());
                coords.push_back(p1.z());
                coords.push_back(1);
                coords.push_back(0);
                coords.push_back(0);
                coords.push_back(norm.x());
                coords.push_back(norm.y());
                coords.push_back(norm.z());

                coords.push_back(p2.x());
                coords.push_back(p2.y());
                coords.push_back(p2.z());
                coords.push_back(1);
                coords.push_back(0);
                coords.push_back(0);
                coords.push_back(norm.x());
                coords.push_back(norm.y());
                coords.push_back(norm.z());

                coords.push_back(p3.x());
                coords.push_back(p3.y());
                coords.push_back(p3.z());
                coords.push_back(1);
                coords.push_back(0);
                coords.push_back(0);
                coords.push_back(norm.x());
                coords.push_back(norm.y());
                coords.push_back(norm.z());
            }

        }
        cout << "headline in the hood nnsdfnsdfnsdfsndfsndfsndfsndfsdfnsdfsnd what is your pronlem homie" << endl;
        cout << minX << " " << minY << " " << minZ << endl;
        cout << maxX << " " << maxY << " " << maxZ << endl;
        float aveX = (minX + maxX)/2;
        float aveY = (minY + maxY)/2;
        float aveZ = (minZ + maxZ)/2;
        float xSize = maxX - minX;
        float ySize = maxY - minY;
        float zSize = maxZ - minZ;
        Matrix scaleTr;
        scaleTr.translate(-aveX,-aveY,-aveZ);
        Matrix scale;
        scale.scale(.2/xSize,.2/ySize,.2/zSize);
        Model obj = Model(coords, Shader("../vert.glsl", "../frag.glsl"));
        Matrix trans;
        trans.translate(-4,-1.4,-4);
        obj.model =  trans*scale*scaleTr;
        return obj;
    }
};

#endif //GLFW_EXAMPLE_LOADMODEL_H
