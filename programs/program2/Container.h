//
// Created by fee_c on 3/27/2018.
//

#ifndef GLFW_EXAMPLE_CONTAINER_H
#define GLFW_EXAMPLE_CONTAINER_H


class Container {
public:
    float xMin;
    float xMax;
    float zMin;
    float zMax;
    bool inside(float x, float z){
        return (xMin < x && xMax > x && zMin < z && zMax > z);
    }
};


#endif //GLFW_EXAMPLE_CONTAINER_H
