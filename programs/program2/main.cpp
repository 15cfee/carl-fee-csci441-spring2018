#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"
#include "loadm.h"
#include "maze.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}
float objX = -4;
float objZ = -4;
const float TRANS = .005;
Maze maze;
Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;


    if (isPressed(window, GLFW_KEY_UP)) {
        if(maze.inBounds(objX,objZ - TRANS)){
            trans.translate(0, 0, -TRANS);
            objZ -= TRANS;
        }
    }
    else if (isPressed(window, GLFW_KEY_DOWN)) {
        if(maze.inBounds(objX,objZ + TRANS)){
            trans.translate(0, 0, TRANS);
            objZ += TRANS;
        }
    }
    else if (isPressed(window, GLFW_KEY_LEFT)) {
        if(maze.inBounds(objX - TRANS,objZ)){
            trans.translate(-TRANS, 0, 0);
            objX -= TRANS;
        }
    }
    else if (isPressed(window, GLFW_KEY_RIGHT)) {
        if(maze.inBounds(objX + TRANS,objZ)){
            trans.translate(TRANS, 0, 0);
            objX += TRANS;
        }
    }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {

    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-program2", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
//    Model obj(
//            Cylinder(40, 1, .2, .4).coords,
//            Shader("../vert.glsl", "../frag.glsl"));

    Load objectLoader;
    //std::vector<float> a = ;
//a, Shader("../vert.glsl", "../frag.glsl")
    Model obj = objectLoader.load("../models/duck.obj");
    //Matrix s;
    //s.scale(.0001,.0001,.0001);

    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));
    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, -2, 0);
    floor_scale.scale(10, 1, 10);
    floor.model = floor_trans*floor_scale;


    maze.load("../text.txt");
    //Model m(maze.coords, Shader("../vert.glsl", "../frag.glsl"));
//    Matrix down;
//    down.translate(0,-1,1);
//    test.model = down;

    // setup camera
    Matrix projection;
    projection.ortho(-4, 4,-4,4,-10,10);
    //projection.ortho(-4, 4,-2,1,.01,10);
//    projection.perspective(45, 1, .01, 10);

    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 3, 0);
    camera.origin = Vector(0, -3, 0);
    camera.up = Vector(0, 0, -1);
//    camera.eye = Vector(0, -1, 1);
//    camera.origin = Vector(0, -2, 0);
//    camera.up = Vector(1, 0, 1);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);

    /* Loop until the user closes the window */
    float y = 0;
    bool ortho = true;
    bool space = false;
    float rot = 0;
    float tilt = 0;
    float angleC = .005;
    float tiltB = 1;
    while (!glfwWindowShouldClose(window)) {
        // process input
        //processInput(floor.model, window);
        processInput(obj.model, window);
        if (isPressed(window, GLFW_KEY_SPACE)) {
            space = true;
        } else if(space){
            space = false;
            ortho = !ortho;
        }
        if(ortho){
            projection.ortho(-4, 4,-4,4,-10,10);
            camera.projection = projection;
            camera.eye = Vector(0, 3, 0);
            camera.origin = Vector(0, -3, 0);
            camera.up = Vector(0, 0, -1);
        } else {
            if (isPressed(window, GLFW_KEY_S) && tilt < .2) {
                tilt += angleC;
            } else if (isPressed(window, GLFW_KEY_W) && tilt > -.4) {
                tilt -= angleC;
            } else if (isPressed(window, GLFW_KEY_D)) {
                rot += angleC;
            } else if (isPressed(window, GLFW_KEY_A)) {
                rot -= angleC;
            }
            projection.perspective(45, 1, .01, 10);
            camera.projection = projection;
            camera.eye = Vector(objX + .5*sin(rot), -1 + tilt, objZ + .5*cos(rot));
            camera.origin = Vector(objX, -1.5, objZ);
            camera.up = Vector(0, 1, 0);
        }
        //camera.up = Vector(0+x,1+y,0+z);
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the floor
        renderer.render(camera, obj, lightPos);
        renderer.render(camera, floor, lightPos);
        //renderer.render(camera, m, lightPos);
        maze.render(renderer,camera,lightPos);

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
