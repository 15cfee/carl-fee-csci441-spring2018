//
// Created by fee_c on 3/26/2018.
//https://stackoverflow.com/questions/201718/concatenating-two-stdvectors

#ifndef GLFW_EXAMPLE_MAZE_H
#define GLFW_EXAMPLE_MAZE_H

#include <fstream>
#include <math.h>
#include <sstream>
#include <iostream>
#include <vector>
#include <csci441/shader.h>
#include "shape.h"
#include "model.h"
#include "renderer.h"
#include "camera.h"
#include "Container.h"

using namespace std;
class Maze {

public:
    vector<float> coords;
    void load(string file){
        string line;
        ifstream input(file);
        std::getline(input, line);
        istringstream iss(line);
        string sub;
        iss >> sub;
        r = strtof(sub.c_str(), nullptr);
        iss >> sub;
        c = strtof(sub.c_str(), nullptr);
        cout << r << " " << c << endl;
        generateScale();
        for (int i = 0; i < r; ++i) {
                std::getline(input, line);
                for (int j = 0; j < c; ++j) {
                        if(line[j] == 'X'){
                                add(i,j);
                        }
                }
        }
    }
    void render(Renderer renderer, Camera camera, Vector lightPos){
            for(Model mod : models){
                    renderer.render(camera,mod,lightPos);
            }
    }
    bool inBounds(float x, float z){
            if(x > BOUNDS || z > BOUNDS || x < -BOUNDS || z < -BOUNDS) return false;
            for(Container tainer: tainers){
                if(tainer.inside(x,z)) return false;
            }
        return true;
    }
private:
    const float DEADBA = 0.1;
    const float BOUNDS = 4.75;
    vector<Model> models;
    vector<Container> tainers;
    int r;
    int c;
    float width;
    float height;
    Matrix down;
    void add(int i, int j){
            DiscoCube cube = DiscoCube();
            Model cur(cube.coords,Shader("../vert.glsl", "../frag.glsl"));
            Matrix scale;
            scale.scale(width,1,height);
            Matrix trans;
            trans.translate(((float)j)*width + width/(float)2 - (float)5,-1,(i)*height + height/2 -5);
            cur.model = trans*scale;
            models.push_back(cur);
            Container tainer;
            tainer.xMin = j*width -5 - DEADBA;
            tainer.xMax = (j+1)*width - 5 + DEADBA;
            tainer.zMin = i*height -5 - DEADBA;
            tainer.zMax = (i+1)*height - 5 + DEADBA;
            tainers.push_back(tainer);
    }
    void generateScale(){
            width = 10.0/c;
            height = 10.0/r;
    }
};


#endif //GLFW_EXAMPLE_MAZE_H
