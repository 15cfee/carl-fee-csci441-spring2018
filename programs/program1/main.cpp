#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <vector>
#include <algorithm>

using namespace std;

/* some sources that I used
 * https://www.codeguru.com/cpp/cpp/cpp_mfc/stl/article.php/c4027/C-Tutorial-A-Beginners-Guide-to-stdvector-Part-1.htm
 * https://stackoverflow.com/questions/7173494/vbos-with-stdvector
 * copied some code about the mouse from https://www.youtube.com/watch?v=EE5cS8EMT78
 * https://msdn.microsoft.com/en-us/library/2dzy4k6e.aspx
 * https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
 * I was googling pretty often and probably missed a couple websites for some general info
 * https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line
 * http://www.cplusplus.com/forum/general/197176/
 * https://stackoverflow.com/questions/3177241/what-is-the-best-way-to-concatenate-two-vectors
 * https://www.geeksforgeeks.org/declare-a-cc-function-returning-pointer-to-array-of-integers/
 * http://www.cplusplus.com/reference/algorithm/sort/
 * https://stackoverflow.com/questions/875103/how-do-i-erase-an-element-from-stdvector-by-index
 * https://stackoverflow.com/questions/3030829/is-it-possible-to-set-an-object-to-null
 *  * https://stackoverflow.com/questions/3450860/check-if-a-stdvector-contains-a-certain-object
 *  https://www.google.com/search?q=inverse+sin+c%2B%2B&rlz=1C1JZAP_enUS775US775&oq=inverse+sin+c%2B%2B&aqs=chrome..69i57j0.6076j0j4&sourceid=chrome&ie=UTF-8
 */

enum Mode {view = 1, stamp, userDefined, group, modify, changeState};

enum Color {red = 0, green, blue};

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

class Shape {
public:
    vector<float> indices;
    int index;
    void init(int size){
        indices = vector<float>(size);
    }
    void translate(float dx, float dy){
        //cout << dx << dy << endl;
        for (int i = 0; i < indices.size()/5; ++i) {
            indices[i*5] += dx;
            indices[i*5 + 1] += dy;
        }
    }
    void scale(float s, float x, float y){
//        float maxX = -100;
//        float minX = 100;
//        float maxY = -100;
//        float minY = 100;
//        for (int i = 0; i < indices.size()/5; ++i) {
//            if(maxX < indices[i*5]){
//                maxX = indices[i*5];
//            }
//            if(minX > indices[i*5]){
//                minX = indices[i*5];
//            }
//            if(maxY < indices[i*5 + 1]){
//                maxY = indices[i*5 + 1];
//            }
//            if(minY > indices[i*5 + 1]){
//                minY = indices[i*5 + 1];
//            }
//        }
//        float avX = (minX + maxX)/2;
//        float avY = (minY + maxY)/2;
        for (int i = 0; i < indices.size()/5; ++i) {
            indices[i*5] = (indices[i*5] - x)*s + x;
            indices[i*5 + 1] = (indices[i*5 + 1] - y)*s + y;
        }
    }
    void rotate(float theta, float centerX, float centerY){
        for (int i = 0; i < indices.size()/5; ++i) {
            float x = indices[i*5] - centerX;
            float y = indices[i*5 + 1] - centerY;
            indices[i*5] = (x*cos(theta) - y*sin(theta)) + centerX;
            indices[i*5 + 1] = x*sin(theta) + y*cos(theta) + centerY;
        }
    }
    void makeTri(float x, float y, float size){
        init(15);
        int a = 0;
        indices[0 + a] = size+x; indices[1 + a] = size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
        a += 5;
        indices[0 + a] = size+x; indices[1 + a] = -size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
        a += 5;
        indices[0 + a] = -size+x; indices[1 + a] = size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
    }
    void makeSquare(float x, float y, float size){
        init(30);
        int a = 0;
        indices[0 + a] = size+x; indices[1 + a] = size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
        a += 5;
        indices[0 + a] = size+x; indices[1 + a] = -size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
        a += 5;
        indices[0 + a] = -size+x; indices[1 + a] = size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;

        a+=5;
        indices[0 + a] = -size+x; indices[1 + a] = -size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
        a += 5;
        indices[0 + a] = size+x; indices[1 + a] = -size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
        a += 5;
        indices[0 + a] = -size+x; indices[1 + a] = size+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
    }
    void makeCircle(float x, float y, float size){
        int n = 20;
        init(n*15);
        int a = 0;
        float theta = 0;
        float inc = 2*3.14159265358979323846/n;
        for (int i = 0; i < n; ++i) {
            indices[0 + a] = size*cos(theta)+x; indices[1 + a] = size*sin(theta)+y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
            a += 5;
            indices[0 + a] = size*cos(theta + inc)+x; indices[1 + a] = size*sin(theta + inc) +y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
            a += 5;
            indices[0 + a] = x; indices[1 + a] = y; indices[2 + a] = 1.0; indices[3 + a] = 0.0; indices[4 + a] = 0.0;
            a+=5;
            theta += inc;
        }
    }
    float earVal = 0;
    void userDef(vector<float> outerWall){

        // 15 points for every triangle
        init((outerWall.size()/2 - 2)*15);
        index = 0;
        // continue until only a triangle is remaining
        while(outerWall.size()/2 > 3){
            // remove an ear
            outerWall = removeEar(&outerWall);
        }
        // handle the last triangle
        for (int i = 0; i < 3; ++i) {
            float x = outerWall[2*i];
            float y = outerWall[2*i + 1];
            // init a point
            indices[index++] = x;
            indices[index++] = y;
            // blue for now
            indices[index++] = 0;
            indices[index++] = 0;
            indices[index++] = 1;
        }
    }
    void changeColor(Color color){
        makeBlack();
        int loc;
        if(color == red){
            loc = 2;
        } else if(color == green){
            loc = 3;
        } else if(color == blue){
            loc = 4;
        }
        for (int i = loc; i < indices.size(); i+=5) {
            indices[i] = 1;
        }
    }
    void makeBlack(){
        for (int i = 2; i < indices.size(); i+=5) {
            indices[i] = 0;
            indices[i + 1] = 0;
            indices[i + 2] = 0;
        }
    }

    void setWhite(){
        for (int i = 2; i < indices.size(); i+=5) {
            indices[i] = 1;
            indices[i + 1] = 1;
            indices[i + 2] = 1;
        }
    }

private:
    vector<float> removeEar(vector<float> *wall){
        vector<float> outerWall = *wall;
        vector<float> newWall(outerWall.size() - 2);
        for (int i = 0; i < outerWall.size()/2 - 1; ++i) {
            int spot = i*2;
            float prevX = outerWall[outerWall.size() - 2];
            float prevY = outerWall[outerWall.size() - 1];
            if(i != 0){
                prevX = outerWall[spot - 2];
                prevY = outerWall[spot - 1];
            }
            float x = outerWall[spot];
            float y = outerWall[spot+ 1];
            float nextX = outerWall[spot + 2];
            float nextY = outerWall[spot + 3];
            float x1Dif = prevX - x;
            float x2Dif = nextX - x;
            float y1Dif = prevY - y;
            float y2Dif = nextY - y;

            float crossP = x1Dif*y2Dif - y1Dif*x2Dif;
            // if ear
            if(crossP < 0 && noPointsInside(&outerWall, spot - 2)){
                // add triangle
                int tempSpot = spot - 2;
                for (int i = 0; i < 3; ++i) {
                    float x = outerWall[2*i + tempSpot];
                    float y = outerWall[2*i + 1 + tempSpot];
                    if(spot == 0 && i == 0){
                        x = outerWall[outerWall.size() -2];
                        y = outerWall[outerWall.size() -1];
                    }
                    // init a point
                    indices[index++] = x;
                    indices[index++] = y;
                    // blue for now
                    indices[index++] = earVal;
                    earVal -= .2;
                    indices[index++] = 0;
                    indices[index++] = 0;
                }
                // remove index from outer wall
                int newIndex = 0;
                // add stuff before index
                for (int j = 0; j < spot; ++j) {
                    newWall[newIndex++] = outerWall[j];
                }
                // add stuff after index
                for (int k = spot + 2; k < outerWall.size(); ++k) {
                    newWall[newIndex++] = outerWall[k];
                }
                break;
            }
        }
        return newWall;
    }
    bool noPointsInside(vector<float> *wall, int index){
        // this function uses the side of the line test
        vector<float> nums = *wall;
        float x1 = nums[index];
        float y1 = nums[index + 1];
        if(index < 0){
            x1 = nums[nums.size() - 2];
            y1 = nums[nums.size() - 1];
        }
        float x2 = nums[index + 2];
        float y2 = nums[index + 3];
        float x3 = nums[index + 4];
        float y3 = nums[index + 5];
        //cout << "triangle coords " << x1 << "," << y1 << ": " << x2 << "," << y2 << ": " << x3 << "," << y3 << endl;
        float vec1x = x1 - x2;
        float vec1y = y1 - y2;
        float vec2x = x2 - x3;
        float vec2y = y2 - y3;
        float vec3x = x3 - x1;
        float vec3y = y3 - y1;
        //cout << "init vectors " << vec1x << "," << vec1y << ": " << vec2x << "," << vec2y << ": " << vec3x << "," << vec3y << endl;
        for (int i = 0; i < nums.size()/2; ++i) {
            int spot = i*2;
            float cross1 = vec1x*(nums[spot + 1] - y1) - vec1y*(nums[spot] - x1);

            float cross2 = vec2x*(nums[spot + 1] - y2) - vec2y*(nums[spot] - x2);

            float cross3 = vec3x*(nums[spot + 1] - y3) - vec3y*(nums[spot] - x3);

            //cout << cross1 << " " << cross2 << " " << cross3 << endl;
            float deadBand = 0.0001;
            if(cross1 > -deadBand) continue;
            if(cross3 > -deadBand) continue;
            if(cross2 > -deadBand) continue;
            return false;
        }
//        for (int i = (index + 3)/2; i < nums.size()/2; i++){
//            int spot = i*2;
//            float cross1 = vec1x*(nums[spot + 1] - y1) - vec1y*(nums[spot] - x1);
//            if(cross1 < 0) return true;
//            float cross2 = vec2x*(nums[spot + 1] - y2) - vec2y*(nums[spot] - x2);
//            if(cross2 < 0) return true;
//            float cross3 = vec3x*(nums[spot + 1] - y3) - vec3y*(nums[spot] - x3);
//            if(cross3 < 0) return true;
//        }

        return true;
    }
};


class Group {
public:

    bool wasSet = false;
    vector<float> overallVertices;
    vector<Group> groups;
    Shape shape;
    int index;
    void trans(float dx, float dy){
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].trans(dx,dy);
        }
        if(wasSet){
            shape.translate(dx,dy);
        }
    }
    void scale(float a){
        float x;
        float y;
        cout << "please enter the pivot x coord " << endl;
        cin >> x;
        cout << x << endl;
        cout << "please enter the pivot y coord " << endl;
        cin >> y;
        cout << y << endl;
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].scale(a,x,y);
        }
        if(wasSet){
            shape.scale(a,x,y);
        }
    }
    void scale(float a, float x, float y){
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].scale(a,x,y);
        }
        if(wasSet){
            shape.scale(a,x,y);
        }
    }
    void rotate(float a, float x, float y){
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].rotate(a, x, y);
        }
        if(wasSet){
            shape.rotate(a, x, y);
        }
    }
    void rotate(float a){
        float x;
        float y;
        cout << "please enter the pivot x coord " << endl;
        cin >> x;
        cout << x << endl;
        cout << "please enter the pivot y coord " << endl;
        cin >> y;
        cout << y << endl;
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].rotate(a, x, y);
        }
        if(wasSet){
            shape.rotate(a, x, y);
        }
    }
    void setShape(Shape shape){
        wasSet = true;
        color = red;
        this->shape = shape;
    }
    void addGroup(Group group){
        groups.push_back(group);
    }
    void render(){
        overallVertices.clear();
        for (int j = 0; j < groups.size(); ++j) {
            vector<float> curGroup = *groups[j].getVertices();
            overallVertices.insert(overallVertices.end(),curGroup.begin(),curGroup.end());
        }
        bufferSize = overallVertices.size()* sizeof(float);
    }
    int bufferSize;

    vector<float> *getVertices(){
        overallVertices.clear();
        if(wasSet){
            for (int i = 0; i < shape.indices.size(); ++i) {
                overallVertices.push_back(shape.indices[i]);
            }
        }
        for (int j = 0; j < groups.size(); ++j) {
            vector<float> curGroup = *groups[j].getVertices();
            overallVertices.insert(overallVertices.end(),curGroup.begin(),curGroup.end());
        }
        return &overallVertices;
    }

    void incrementColor(){
        color = static_cast<Color>((color + 1)%3);
        shape.changeColor(color);
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].changeColor(color);
        }
    }
    void changeColor(Color color){
        this->color = color;
        shape.changeColor(color);
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].changeColor(color);
        }
    }
    void decrementColor(){
        color = static_cast<Color>((color + 5)%3);
        shape.changeColor(color);
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].changeColor(color);
        }
    }

    void reGroup(vector<int> collection){
        if(collection.size() <= 1) return;
        sort(collection.begin(),collection.end());
        Group col;
        for (int i = collection.size() - 1; i >= 0; --i) {
            int groupNum = collection[i];
            col.addGroup(groups[groupNum]);
            if(groups[groupNum].wasSet){
                col.color = groups[groupNum].color;
            }
            groups.erase(groups.begin() + groupNum);
        }
        groups.push_back(col);
    }

    void disband(int index){
        Group toDis = groups[index];
        if(toDis.groups.empty()) return;
        for (int i = 0; i < toDis.groups.size(); ++i) {
            groups.push_back(toDis.groups[i]);
        }
        groups.erase(groups.begin() + index);
    }

    void whiteIndication(){
        shape.setWhite();
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].whiteIndication();
        }
    }

    void blackIndication(){
        shape.makeBlack();
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].blackIndication();
        }
    }

    void restore(){
        for (int i = 0; i < groups.size(); ++i) {
            groups[i].restore();
        }
        if(wasSet){
            shape.changeColor(color);
        }
    }

private:
    Color color;
};



class Matrix {
private:

public:
    //int const matD = 3;
    float matrix [3][3];
    int val1 = 20;
    void setIdent(){
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                if(j == i ){
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = 0;
                }
            }
        }
    }

    void setZero(){
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                matrix[i][j] = 0;
            }
        }
    }

    void trans(float x, float y){
        Matrix tempTrans;
        tempTrans.setIdent();
        tempTrans.matrix[0][2] = x;
        tempTrans.matrix[1][2] = -y;
        *this = tempTrans * *this;
    }

    void setZoom(float zoom) {
        setIdent();
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                matrix[i][j] *= zoom;
            }
        }
    }

    Matrix operator*(Matrix mult){
        Matrix temp;
        temp.setZero();
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                for (int k = 0; k < 3; ++k) {
                    temp.matrix[i][j] += matrix[i][k]*mult.matrix[k][j];
                }
            }
        }
        return temp;
    }
    void print(){
        for (int i = 0; i < 3; ++i) {
            std::cout << matrix[i][0] << " " << matrix[i][1] << " " << matrix[i][2] << std::endl;
        }
    }
};

// copied the sample project from part three to get going


// youtube
static void cursorPositionCallback(GLFWwindow *window, double xPos, double yPos);
void mouseButtonCallback(GLFWwindow * window, int button, int action, int mods);

// allows outsourcing to methods
GLuint VBO[1];
GLuint VAO[1];
Group outer;
float xDif = -100;
float yDif = -100;
void regenerate();
void exitStage();

// this is global so that it is modified on cursor movement
Matrix translate;
bool leftPressed = false;
int curGroupIndex;
bool valueUsed;
float lastX;
float lastY;

// keeps track of the state
Mode state;

int main(void) {
    // start in changeState mode
    state = changeState;

    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Program 1", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    // init some things for translating based on mouse movement
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback );
    translate.setIdent();


    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // binds a bunch of buffer stuff
    regenerate();

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
    // use the shader
    shader.use();

    // time variable
    float timeValue;
    /* Loop until the user closes the window */
    bool toggle = false;

    // translate init
    GLuint transLoc = glGetUniformLocation(shader.id(), "translate");
    glUniformMatrix3fv(transLoc, 1, true, &translate.matrix[0][0]);

    // init message
    cout << "pressing the following buttons would correspond to the respective states:" << endl;
    cout << "view = v, stamp = s, userDefined = u, group = g, modify = m, changeState = q" << endl;
    // s reused in stamp
    bool sPressed = true;
    // user
    bool happened = false;

    // grouping
    vector<int> selectedGroups;
    curGroupIndex = 0;
    bool d = false;

    // vars for modification
    bool left = false;
    bool right = false;
    bool one = false;
    bool two = false;
    bool three = false;
    bool four = false;

    // zoom vars
    Matrix zoom;
    float scaling = 1;
    zoom.setZoom(scaling);
    GLuint scaleLocation = glGetUniformLocation(shader.id(), "scale");
    glUniformMatrix3fv(scaleLocation, 1, false, &zoom.matrix[0][0]);
    bool up = false;
    bool down = false;
    // get cur time
    timeValue = glfwGetTime();
    while (!glfwWindowShouldClose(window)) {


        //check if modes should be swapped
        // view = v, stamp = s, userDefined = u, group = g, modify = m, changeState = q
        if(GetAsyncKeyState(0x56) && state == changeState){
            cout << "switching to view state" << endl;
            cout << "use up and down arrow keys to zoom in or out" << endl;
            cout << "press and drag the left mouse button on the window to pan" << endl;
            state = view;
        } else if(GetAsyncKeyState(0x53) && state == changeState){
            cout << "switching to stamp state" << endl;
            cout << "press:" << endl;
            cout << "'T' to create a triangle" << endl;
            cout << "'S' to create a square" << endl;
            cout << "'C' to create a circle" << endl;
            sPressed = true;
            state = stamp;
        } else if(GetAsyncKeyState(0x55) && state == changeState){
            cout << "switching to userDefined state" << endl;
            state = userDefined;
        } else if(GetAsyncKeyState(0x47) && state == changeState){
            cout << "switching to group state" << endl;
            cout << "use the left and right arrow to move between groups" << endl;
            cout << "white marks the current groups" << endl;
            cout << "press the up arrow to select a group " << endl;
            cout << "when all the desired groups (black) have been selected," << endl;
            cout << "press the down arrow to make them into a group" << endl;
            cout << "press d to disband the group" << endl;
            state = group;
        } else if(GetAsyncKeyState(0x4D) && state == changeState){
            cout << "switching to modify state" << endl;
            cout << "press one and two to zoom in and out respectively" << endl;
            cout << "press three and four to rotate cc and cw respectively" << endl;
            cout << "use the mouse to drag a group" << endl;
            cout << "press the up and down arrow keys to change the color of groups" << endl;
            cout << "press the left and right arrow keys to select the next group" << endl;
            state = modify;
        } else if(GetAsyncKeyState(0x51) && state != changeState){
            exitStage();
        }

        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        if(state == view){
            // this handles the viewport zoom
            if(GetAsyncKeyState(VK_UP)) {
                up = true;
            } else if(up){
                up = false;
                scaling += .04;
                zoom.setZoom(scaling);
                GLuint scaleLocation = glGetUniformLocation(shader.id(), "scale");
                glUniformMatrix3fv(scaleLocation, 1, false, &zoom.matrix[0][0]);
            } else if(GetAsyncKeyState(VK_DOWN)){
                down = true;
            } else if(down){
                down = false;
                scaling -= .04;
                zoom.setZoom(scaling);
                GLuint scaleLocation = glGetUniformLocation(shader.id(), "scale");
                glUniformMatrix3fv(scaleLocation, 1, false, &zoom.matrix[0][0]);
            }
        } else if(state == stamp){
            // buttons for different stamps: t = triangle, c = circle, s = square
            if(GetAsyncKeyState(0x54)){
                cout << "creating a triangle" << endl;
                float x;
                float y;
                float zoom;
                float rot;
                cout << "please enter the center x-coord: " << endl;
                cin >> x;
                cout << x << endl;
                cout << "please enter the center y-coord: " << endl;
                cin >> y;
                cout << y << endl;
                cout << "please enter the scale: " << endl;
                cin >> zoom;
                cout << zoom << endl;
                Shape tri2;
                tri2.makeTri(x,y,zoom);
                Group group;
                group.setShape(tri2);
                cout << "please enter the rotation amount " << endl;
                cin >> rot;
                cout << rot << endl;
                group.rotate(rot);
                outer.addGroup(group);
                outer.render();
                // bunch of rebinding stuff
                regenerate();
                // exit stamp mode after each shape
                exitStage();
            } else if(GetAsyncKeyState(0x43)){
                cout << "creating a circle" << endl;
                float x;
                float y;
                float zoom;
                float rot;
                cout << "please enter the center x-coord: " << endl;
                cin >> x;
                cout << x << endl;
                cout << "please enter the center y-coord: " << endl;
                cin >> y;
                cout << y << endl;
                cout << "please enter the scale: " << endl;
                cin >> zoom;
                cout << zoom << endl;
                Shape circle;
                circle.makeCircle(x,y,zoom);
                Group next;
                next.setShape(circle);
                cout << "please enter the rotation amount " << endl;
                cin >> rot;
                cout << rot << endl;
                next.rotate(rot);
                outer.addGroup(next);
                outer.render();
                regenerate();
                exitStage();
            } else if(GetAsyncKeyState(0x53) && !sPressed){
                cout << "creating a square" << endl;
                float x;
                float y;
                float zoom;
                float rot;
                cout << "please enter the center x-coord: " << endl;
                cin >> x;
                cout << x << endl;
                cout << "please enter the center y-coord: " << endl;
                cin >> y;
                cout << y << endl;
                cout << "please enter the scale: " << endl;
                cin >> zoom;
                cout << zoom << endl;
                Shape square;
                square.makeSquare(x,y,zoom);
                Group group;
                group.setShape(square);
                cout << "please enter the rotation amount " << endl;
                cin >> rot;
                cout << rot << endl;
                group.rotate(rot);
                outer.addGroup(group);
                outer.render();
                regenerate();
                exitStage();
                sPressed = true;
            } else if(!GetAsyncKeyState(0x53)){
                sPressed = false;
            }
        } else if(state == userDefined && !happened){
            //happened = true;
            cout << "how many vertices do you want to enter " << endl;
            int input;
            cin >> input;
//            input = 4;
            Shape test2;
            vector<float> outerWall2(input * 2);
            for (int i = 0; i < input; ++i) {
                cout << "select a point with the mouse" << endl;
                cout << "make sure to wiggle the mouse a bit when it is pressed down" << endl;
                cout << "the shape points should be picked in a counterClockwise direction" << endl;
                valueUsed = true;
                while(valueUsed){
                    glfwPollEvents();
                    processInput(window);
                }
                outerWall2[i*2] = (lastX - 320)/320; // 640
                outerWall2[i*2 + 1] = -(lastY - 240)/240; // 480
                while(leftPressed){
                    glfwPollEvents();
                    processInput(window);
                }
                valueUsed = true;
                cout << "point " << (i + 1) <<" selected" << endl;
            }
            float zoom;
            cout << "please enter the scale: " << endl;
            cin >> zoom;
            cout << zoom << endl;
            test2.userDef(outerWall2);
            Group group2;
            group2.setShape(test2);
            group2.scale(zoom);
            cout << "please enter the rotation amount " << endl;
            float rot;
            cin >> rot;
            cout << rot << endl;
            group2.rotate(rot);
            outer.addGroup(group2);
            outer.render();
            regenerate();
            exitStage();
        } else if(state == group){
            if(GetAsyncKeyState(VK_LEFT)){
                left = true;
            } else if(left){
                left = false;
                if(selectedGroups.empty() || find(selectedGroups.begin(),selectedGroups.end(),curGroupIndex) == selectedGroups.end()){
                    outer.groups[curGroupIndex].restore();
                }
                curGroupIndex = (curGroupIndex - 1)%outer.groups.size();
                if(selectedGroups.empty() || find(selectedGroups.begin(),selectedGroups.end(),curGroupIndex) == selectedGroups.end()){
                    outer.groups[curGroupIndex].whiteIndication();
                }
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(VK_RIGHT)){
                right = true;
            } else if(right){
                right = false;
                if(selectedGroups.empty() || find(selectedGroups.begin(),selectedGroups.end(),curGroupIndex) == selectedGroups.end()){
                    outer.groups[curGroupIndex].restore();
                }
                curGroupIndex = (curGroupIndex + 1)%outer.groups.size();
                if(selectedGroups.empty() || find(selectedGroups.begin(),selectedGroups.end(),curGroupIndex) == selectedGroups.end()){
                    outer.groups[curGroupIndex].whiteIndication();
                }
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(VK_UP)){
                up = true;
            } else if(up) {
                up = false;
                outer.groups[curGroupIndex].blackIndication();
                selectedGroups.push_back(curGroupIndex);
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(VK_DOWN)){
                down = true;
            } else if(down){
                down = false;
                outer.reGroup(selectedGroups);
                selectedGroups.clear();
                curGroupIndex = 0;
                outer.restore();
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(0x44)){
                d = true;
            } else if(d && selectedGroups.empty()){
                d = false;
                outer.disband(curGroupIndex);
            }
        } else if(state == modify){
            if(GetAsyncKeyState(VK_LEFT)){
                left = true;
            } else if(left){
                left = false;
                curGroupIndex = (curGroupIndex - 1)%outer.groups.size();
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(VK_RIGHT)){
                right = true;
            } else if(right){
                right = false;
                curGroupIndex = (curGroupIndex + 1)%outer.groups.size();
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(VK_UP)){
                up = true;
            } else if(up){
                up = false;
                outer.groups[curGroupIndex].incrementColor();
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(VK_DOWN)){
                down = true;
            } else if(down){
                down = false;
                outer.groups[curGroupIndex].decrementColor();
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(0x31)){
                one = true;
            } else if(one){
                one = false;
                outer.groups[curGroupIndex].scale(.8);
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(0x32)){
                two = true;
            } else if(two){
                two = false;
                outer.groups[curGroupIndex].scale(1.25);
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(0x33)){
                three = true;
            } else if(three){
                three = false;
                outer.groups[curGroupIndex].rotate(.2);
                outer.render();
                regenerate();
            } else if(GetAsyncKeyState(0x34)){
                four = true;
            } else if(four){
                four = false;
                outer.groups[curGroupIndex].rotate(-.2);
                outer.render();
                regenerate();
            }else if(!outer.groups.empty() && abs(xDif) < 1 && (glfwGetTime() - timeValue > .03)){
                //cout << "trying to trans" << endl;
                outer.groups[curGroupIndex].trans(xDif,-yDif);
                outer.render();
                regenerate();
                timeValue = glfwGetTime();
            }
        }
        GLuint transLoc = glGetUniformLocation(shader.id(), "translate");
        glUniformMatrix3fv(transLoc, 1, true, &translate.matrix[0][0]);
        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, outer.bufferSize);

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();

    }

    glfwTerminate();
    return 0;
}
float oldX = -1;
float oldY = -1;
static void cursorPositionCallback(GLFWwindow *window, double xPos, double yPos){
    //cout<< "x " << xPos << " y: " << yPos << endl;
    if(leftPressed){
        if(oldX > -.5){
            xDif = (xPos - oldX)/1000;
            yDif = (yPos - oldY)/1000;
            if(state == view) {
                translate.trans(xDif, yDif);
            }
        }
        oldX = xPos;
        oldY = yPos;
        if(valueUsed && state == userDefined){
            lastX = xPos;
            lastY = yPos;
            valueUsed = false;
        }
    } else {
        oldX = -1;
        oldY = -1;
        xDif = -100;
        yDif = -100;
    }
}
void mouseButtonCallback(GLFWwindow * window, int button, int action, int mods){
    if( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
        //cout << "left pressed" << endl;
        leftPressed = true;
    } else if( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE){
        leftPressed = false;
    }
}

void regenerate(){
    // create and bind the vertex buffer object and copy the data to the buffer
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, outer.bufferSize, &outer.overallVertices[0], GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    //GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);
}

void exitStage() {
    cout << "exiting the current state " << endl << endl;
    cout << "pressing the following buttons would correspond to the respective states:" << endl;
    cout << "view = v, stamp = s, userDefined = u, group = g, modify = m, exit current state = q" << endl;
    state = changeState;
    outer.restore();
    outer.render();
    regenerate();
}
