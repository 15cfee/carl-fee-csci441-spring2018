#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;
uniform mat3 scale;
uniform mat3 translate;

out vec3 myColor;

void main() {
    vec3 transform = scale * translate * vec3(aPos.x, aPos.y, 1);
    gl_Position = vec4(transform.x, transform.y, 0.0, 1.0);
    myColor = aColor;
}
